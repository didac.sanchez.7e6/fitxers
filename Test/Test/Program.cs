﻿using System;

namespace Test
{
    class Program
    {
        static void Main(string[] args)
        {
            var menu = new Program();
            menu.SequenciaAsteriscos();
        }
        public void PrintPiramide(int alsadaMaxima, int curent = 1, bool flag = true)
        {
            if (curent < alsadaMaxima)
            {
                if (flag)
                {
                    Console.WriteLine("*");
                    flag = !flag;
                    PrintPiramide(alsadaMaxima, curent, flag);
                    flag = !flag;
                }
                if (!flag)
                {
                    for (int i = 0; i <= curent; i++)
                    {
                        Console.Write("*");
                        if (i == curent) Console.WriteLine("");
                    }
                    flag = !flag;
                    curent++;
                    PrintPiramide(alsadaMaxima, curent, flag);
                    flag = !flag;
                }
            }
        }
        /*¬*//*-*//*u*//*U*//*w*//*W*/
        public void SequenciaAsteriscos()
        {
            Console.Clear();
            int numeroCalcular = AskNumero();
            PrintPiramide(numeroCalcular);
            EndProgram();
        }
        public void Print(int [] valores)
        {

        }
        public void Print(string[] valores)
        {

        }
        int AskNumero()
        {
            bool flag;
            int numero;
            do
            {
                string guess = (Console.ReadLine());
                flag = int.TryParse(guess, out numero);
            } while (!flag);
            return numero;
        }
        public void EndProgram()
        {
            Console.ReadLine();
            Console.Clear();
        }
    }
}

﻿using System;
/*
* AUTHOR: Didac Sanchez Cullell
* DATE: 2023/01/10
*/


namespace Metodes
{
    class Metodes
    {
        static void Main(string[] args)
        {
            var menu = new Metodes();
            menu.Menu();
        }
        public void Menu()
        {
            string opcio;
            do
            {
                Console.WriteLine("0.Exit");
                Console.WriteLine("1.RigtTriangleSize");
                Console.WriteLine("2.Lamp");
                Console.WriteLine("3.CampSiteOrganizer");
                Console.WriteLine("4.BasicRobot");
                Console.WriteLine("5.ThreeInArow");
                Console.WriteLine("6.SquashCounter");
                opcio = Console.ReadLine();
                MenuOpcions(opcio);
            } while (opcio != "0");
        }
        public void MenuOpcions(string opcio)
        {
            switch (opcio)
            {
                case "0":
                    break;
                case "1":
                    RigtTriangleSize();
                    break;
                case "2":
                    Lamp();
                    break;
                case "3":
                    CAMPSITEORGANIZER();
                    break;
                case "4":
                    BasicRobot();
                    break;
                case "5":
                    ThreeInArow();
                    break;
                case "6":
                    SquashCounter();
                    break;
                default:
                    Console.Clear();
                    break;
            }
        }
        public void RigtTriangleSize()
        {
            Console.Clear();
            Console.WriteLine("Quants triangles vols calcular");
            int numTriangles = (int)Math.Floor(AskNumero());
            double[] resultats = new double[numTriangles *2];
            for (int i = 0; i < resultats.Length; i+=2)
            {
                Console.WriteLine("Quina es la base");
                double trianglrBase = AskNumero();
                Console.WriteLine("Quina es la altura");
                double trianglrAltura = AskNumero();
                resultats [i] = trianglrBase;
                resultats[i + 1] = trianglrBase;
            }
            for (int i = 0; i < resultats.Length; i+=2)
            {
                Console.WriteLine($"Un triangle de {resultats[i]} x {resultats[i+1]} té {resultats[i]*resultats[i+1]} d'àrea i XX de perímetre.");
            }
            Console.ReadLine();
            Console.Clear();
        }
        public void Lamp()
        {

        }
        public void CAMPSITEORGANIZER()
        {
            int personas = 0, parcelas = 0;
            string movimiento, lista="";
            Console.Clear();
            do
            {
                Console.WriteLine("Que esta pasando");
                movimiento = AskString();
                string[] aciones = movimiento.Split(" ");
                if (aciones[0] == "ENTRA")
                {
                    personas += Convert.ToInt32(aciones[1]);
                    parcelas++;
                    Console.WriteLine($"parcel·les: {parcelas} \n persones: {personas}");
                    lista += aciones[1] +" "+ aciones[2] + ",";
                    
                    Console.WriteLine(lista);

                }
                if (aciones[0] == "MARXA")
                {
                    
                    string[] subLista = lista.Split(",");
                    for (int i = 0; i < subLista.Length; i++)
                    {
                        if (subLista[i].Contains(aciones[1]) || parcelas<0)
                        {
                            parcelas--;
                            string[] numPersonasPersona = subLista[i].Split(" ");
                            personas -= Convert.ToInt32(numPersonasPersona[0]);
                            break;
                        }
                    }
                    Console.WriteLine($"parcel·les: {parcelas} \n persones: {personas}");

                }
            } while (movimiento != "END");
            Console.ReadLine();
            Console.Clear();
        }
        public void BasicRobot()
        {

        }
        public void ThreeInArow()
        {

        }
        public void SquashCounter()
        {

        }
        double AskNumero()
        {
            double numero = Convert.ToDouble(Console.ReadLine());
            return numero;
        }
        string AskString()
        {
            string frase = Console.ReadLine();
            frase = frase.ToUpper();
            return frase;
        }
    }
}

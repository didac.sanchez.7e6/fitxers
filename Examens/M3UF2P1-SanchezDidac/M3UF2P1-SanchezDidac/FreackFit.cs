﻿using System;
using System.Linq;

namespace M3UF2P1_SanchezDidac
{
    class FreackFit
    {
        static void Main()
        {
            string hardCoding = "78 99 71 81 96 66 98 97 65 100 67 66 77 79 122 122 115 109 105 130 100 109 108 129 130 110 101 2 129 120 110 116 122 107 120 127 129 104 120 111 124 118 107 107 107 113 109 114 111 129 114 115 119 132 167 157 162 142 141 137 148 137 155 135 149 147 156 145 165 133 155 151 143 159 138 162 155 168 132 170 166 145 159 143 155 130 162 152 165 163 148 147 165 157 145 153 142 133 205 201 176 173 192 188 174 203 184 208 204 186 196 208 156 160 131 169 142 138 169 163 150 164 131 167 153 151 130 132 163 149 163 154 170 170 151 161 144 136 160 100 118 121 106 114 120 116 118 115 125 102 110 102 111 129 104 119 105 112 111 127 122 107 112 118 106 103 103 125 116 112 117 88 85 96 84 79 91 70 71 76 96 77 91 66 84 ";
            string[] dades = hardCoding.Split(" ");
            int[] change = ConverStringInInt(dades);
            var menu = new FreackFit();
            menu.Menu(change);
        }
        public void Menu(int[] dades)
        {
            string opcio;
            do
            {
                Console.WriteLine("0.Exit");
                Console.WriteLine("1.Dades");
                Console.WriteLine("2.Quantitat de dades registrades");
                Console.WriteLine("3.Freqüència mitjana");
                Console.WriteLine("4.Freqüència maxima i minima");
                Console.WriteLine("5.Aprofitamen de l'activitat");
                opcio = Console.ReadLine();
                MenuOpcions(opcio, dades);
            } while (opcio != "0");
        }
        public void MenuOpcions(string opcio, int[] dades)
        {
            switch (opcio)
            {
                case "0":
                    break;
                case "1":
                    Mostrar(dades);
                    EndProgram();
                    break;
                case "2":
                    Mostrar(Quantiata(dades));
                    EndProgram();
                    break;
                case "3":
                    Mostrar(Mitjana(dades));
                    EndProgram();
                    break;
                case "4":
                    Mostrar(MaxMin(dades));
                    EndProgram();
                    break;
                case "5":
                    SubMenu(dades);
                    break;
                default:
                    Console.Clear();
                    break;
            }

        }
        public void Mostrar(int[] dades)
        {
            for (int i = 0; i < dades.Length; i++)
            {
                if (dades[i] != 0) Console.Write($"{dades[i] }\t");
            }
        }
        static int[] ConverStringInInt(string[] dades)
        {
            int[] change = new int[dades.Length - 1];
            for (int i = 0; i < dades.Length - 1; i++)
            {
                change[i] = Convert.ToInt32(dades[i]);
            }
            return change;
        }
        int[] Quantiata(int[] dades)
        {
            int[] dadesProcesades = { dades.Length };
            return dadesProcesades;
        }
        int[] Mitjana(int[] dades)
        {
            int[] dadesProcesades = { dades.Sum() / dades.Length };
            return dadesProcesades;
        }
        int[] MaxMin(int[] dades)
        {
            int[] dadesProcesades = { dades.Max(), dades.Min() };
            return dadesProcesades;
        }
        public void SubMenu(int[] dades)
        {
            string opcio;
            int edat = 0;
            do
            {
                Console.WriteLine("0.Exit");
                Console.WriteLine("1.Intruduex la teva edat");
                Console.WriteLine("2.Pulsasions suaus");
                Console.WriteLine("3.Pulsasions moderades");
                Console.WriteLine("4.Pulsasions altes");
                Console.WriteLine("5.Pulsasions per sorte de suau");
                Console.WriteLine("6.Pulsasions persobre de alt");
                opcio = Console.ReadLine();
                edat = SubMenuOpcions(opcio, dades, edat);
            } while (opcio != "0");
        }
        public int SubMenuOpcions(string opcio, int[] dades, int edat)
        {
            switch (opcio)
            {
                case "0":

                case "1":
                    edat = AskNum(edat);
                    EndProgram();
                    return edat;
                case "2":
                    Console.WriteLine($"{QalcularLecturas(dades, (220 - edat) * 0.5, (220 - edat) * 0.69)}, {(QalcularLecturas(dades, (220 - edat) * 0.5, (220 - edat) * 0.69) / dades.Length) * 100}%");
                    EndProgram();
                    return edat;
                case "3":
                    Console.WriteLine($"{QalcularLecturas(dades, (220 - edat) * 0.7, (220 - edat) * 0.89)}, {(QalcularLecturas(dades, (220 - edat) * 0.7, (220 - edat) * 0.89) / dades.Length) * 100}%");
                    EndProgram();
                    return edat;
                case "4":
                    Console.WriteLine($"{QalcularLecturas(dades, (220 - edat) * 0.9, (220 - edat))}, {(QalcularLecturas(dades, (220 - edat) * 0.5, (220 - edat)) / dades.Length) * 100}%");
                    EndProgram();
                    return edat;
                case "5":
                    Mostrar((QalcularLecturasNumerosEnteros(dades, 0, (220 - edat) * 0.5)));
                    EndProgram();
                    return edat;
                case "6":
                    Mostrar(QalcularLecturasNumerosEnteros(dades, 220 - edat, 9999999999999));
                    EndProgram();
                    return edat;
                default:
                    Console.Clear();
                    return edat;
            }

        }
        int QalcularLecturas(int[] dades, double min, double max)
        {
            int contador = 0;
            foreach (int pulsacio in dades)
            {
                if (pulsacio < max && pulsacio > min) contador++;
            }
            return contador;
        }
        int[] QalcularLecturasNumerosEnteros(int[] dades, double min, double max)
        {
            int[] contador = new int[dades.Length];
            for (int i = 0; i < dades.Length; i++)
            {
                if (dades[i] < max && dades[i] > min) contador[i] = dades[i];
                else contador[i] = 0;
            }
            return contador;
        }
        int AskNum(int edad)
        {
            string guess;
            do
            {
                guess = Console.ReadLine();
            } while (!int.TryParse(guess, out edad) && edad > 15 && edad < 220);
            return edad;
        }
        public void EndProgram()
        {
            Console.ReadLine();
            Console.Clear();
        }
    }
}

﻿using System;

namespace Asteriscos
{
    class Asteriscos
    {
        static void Main()
        {
           int longitut  = AskNum();
            Core(longitut);
        }
        static int AskNum()
        {
            string guess;
            int num;
            do
            {
                guess = Console.ReadLine();
            } while (!int.TryParse(guess, out num));
            return num;
        }
        static void Core(int longitut)
        {
            if (longitut - 1 != 0) Core(longitut - 1);
            if (longitut - 1 != 0) Core(longitut - 1);
            for (int i = 0; i < longitut; i++)
            {
                Console.Write("*");
            }
            Console.WriteLine("");
            
        }
    }
}

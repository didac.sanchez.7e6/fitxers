﻿using System;

namespace Fibonacci
{
    class Fibonacci
    {
        static void Main(string[] args)
        {
            int saltos = AskNum();
            Console.WriteLine(Sum(1, saltos));
        }
        static int AskNum()
        {
            string guess;
            int num;
            do
            {
                guess = Console.ReadLine();
            } while (!int.TryParse(guess, out num));
            return num;
        }
        static int Sum(int inico, int saltos)
        {
            if (saltos - 1 > 0) return inico = Sum(inico, saltos - 1) + Sum(inico, saltos - 2);
            else if (saltos  == 0) return 0;
            return 1;
        }
    }
}

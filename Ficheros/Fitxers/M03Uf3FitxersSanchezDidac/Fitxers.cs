﻿using System;
using System.IO;
using System.Collections.Generic;
namespace M03Uf3FitxersSanchezDidac
{
    class Fitxers
    {
        static void Main()
        {
            Menu();
        }
        static void Menu()
        {
            string opcio;
            string[] titulos = { "Exit", "ParellSenar", "Inventario", "WORDCOUNT", "ConfigFile", "FindPaths" };
            do
            {
                foreach (string titulo in titulos)
                {
                    Console.WriteLine(titulo);
                }
                opcio = MenuOpcions(AskString("Quiena opsio vol?"));
            } while (opcio != "0");

        }
        static string MenuOpcions(string opcio)
        {
            switch (opcio)
            {
                case "0":
                    return opcio;
                case "1":
                    ParellSenar();
                    return opcio;
                case "2":
                    Inventario();
                    return opcio;
                case "3":
                    WORDCOUNT();
                    return opcio;
                case "4":
                    ConfigFile();
                    return opcio;
                case "5":
                    FindPaths();
                    return opcio;
                default:
                    Console.Clear();
                    return opcio;
            }
        }
        static void RecursFindPaths(string path, string fiche)
        {
            List<string> directoris = new List<string>();
            if (directoris.Count == 0)
            {
                directoris.AddRange(Directory.EnumerateDirectories(path));
                directoris.AddRange(Directory.EnumerateFiles(path));
            }
            foreach (string directorio in directoris)
            {
                if (File.Exists(directorio) && directorio.Split(@"\")[^1] == fiche)
                {
                    Console.WriteLine(@$"S'ha trobat el fitxer a: {directorio} ");
                }
                if (Directory.Exists(directorio))
                {

                    RecursFindPaths(directorio, fiche);
                }

            }
        }
        static void FindPaths()
        {
            string fiche = AskString("Quin és el nom del fitxer a cercar?");
            string path = AskString(">Escriu el nom d'una ruta a una carpeta:");
            RecursFindPaths(path, fiche);
        }
        static void ConfigFile()
        {
            string path = @"E:\Didac Sanchez Cullell - DAMv\M03\Ficheros\Files";
            string[] lenguage;
            do
            {
                lenguage = AskString("Que lenguage quieres utilizar (Idioma x)").Split(" ");
            } while (lenguage.Length != 2 && lenguage[0].ToLower() != "Idioma".ToLower());
            StreamReader sr = File.OpenText(path);
            List<string> conf = new List<string> { };
            string line;
            while ((line = sr.ReadLine()) != null)
            {
                conf.Add(line);
            }
            sr.Close();
            if (File.Exists(@$"{path}\lang_{lenguage[2]}"))
            {
                sr = File.OpenText(@$"{path}\lang_{lenguage[2]}");
                Console.Write($"{sr.ReadLine()}");
                sr.Close();
            }
            else
            {
                sr = File.OpenText(@$"{path}\lang_{conf[1].Split(":")[1]}");
                Console.Write($"{sr.ReadLine()}");
                sr.Close();
            }
            Console.WriteLine(conf[0].Split(":")[1]);
        }

        static void WORDCOUNT()
        {
            string path = @"E:\Didac Sanchez Cullell - DAMv\M03\Ficheros\Files\wordcount.txt";

            StreamReader sr = File.OpenText(AskPath(path));
            string[] busacar = AskString("Que palabra busacas").Trim().ToLower().Split(" ");
            string[] encontrar = sr.ReadToEnd().Trim().ToLower().Split(" ");
            for (int i = 0; i < busacar.Length; i++)
            {
                int igual = 0;
                foreach (string palabra in encontrar)
                {
                    if (palabra.Contains(busacar[i])) igual++;
                }
                Console.WriteLine($"{busacar[i]} = {igual}");
            }
        }

        static string AskPath(string path)
        {
            if (!File.Exists(path))
            {
                do
                {
                    AskString("El path donde esta el fichero");
                } while (!File.Exists(path));
            }
            return path;
        }
        static void Inventario()
        {
            string path = @"E:\Didac Sanchez Cullell - DAMv\M03\Ficheros\Files\Inventario.txt";
            AskPath(path);
            string[] ask = { ">Introduce el nombre:", ">Introduce el modelo:", ">Introduce el MAC:", ">Introduce el año de fabricación:" };
            StreamWriter writer = new StreamWriter(path);     //File.OpenWrite(path);
            for (int i = 0; i < ask.Length - 1; i++)
            {
                writer.Write(AskString(ask[i]) + ",");
                if (i == ask.Length - 2) writer.WriteLine(AskString(ask[i]));
            }
            writer.Close();
        }

        static void ParellSenar()
        {
            string path = @"E:\Didac Sanchez Cullell - DAMv\M03\Ficheros\Files\Numbers.txt";
            do
            {
                if (!File.Exists(path))
                {
                    path = AskString("Quin es el path del fiche Numbers.txt");
                }
            } while (!File.Exists(path));
            StreamReader sr = File.OpenText(path);
            List<string> numbers = new List<string>();
            string line;
            while ((line = sr.ReadLine()) != null)
            {
                numbers.Add(line);
            }
            sr.Close();
            numbers.Reverse();
            for (int i = 0; i < numbers.Count; i++)
            {
                if (IsParell(numbers[i]))
                {
                    Write(numbers[i], NewPath(path.Split(@"\")) + "Parell.txt");
                }
                else
                {
                    Write(numbers[i], NewPath(path.Split(@"\")) + "Senar.txt");
                }
            }
        }
        static string NewPath(string[] arrayPath)
        {
            string newPath = "";
            for (int i = 0; i < arrayPath.Length - 1; i++)
            {
                newPath += arrayPath[i] + @"\";
            }
            return newPath;
        }
        static void Write(string escribir, string path)
        {
            StreamWriter writer;
            if (File.Exists(path))
            {
                writer = File.AppendText(path);
            }
            else
            {
                writer = new StreamWriter(File.Create(path));
            }
            writer.WriteLine(escribir);
            writer.Close();

        }
        static bool IsParell(string line)
        {
            return (Convert.ToInt16(line) % 2 == 0);
        }
        static string AskString(string pregunta)
        {
            Console.WriteLine(pregunta);
            return Console.ReadLine();
        }
    }
}

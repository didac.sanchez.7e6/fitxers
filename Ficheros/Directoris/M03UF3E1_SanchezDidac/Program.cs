﻿using System;
using System.IO;
using System.Collections.Generic;

namespace M03UF3E1_SanchezDidac
{
    class Program
    {
        static void Main(string[] args)
        {
            Menu();
        }
        static void Menu()
        {
            string opcio;
            do
            {
                Console.WriteLine("0.Exit");
                Console.WriteLine("1.Encontrar un directorio");
                Console.WriteLine("2.Ensenyar contigut de un dicterio");
                Console.WriteLine("3.Borrar Crear Renombrar directorios");
                Console.WriteLine("4.dir/s");
                opcio = Console.ReadLine();
                MenuOpcions(opcio);
            } while (opcio != "0");
        }
        static void MenuOpcions(string opcio)
        {
            switch (opcio)
            {
                case "0":
                    break;
                case "1":
                    Find();
                    break;
                case "2":
                    Ensenyar(AskString());
                    break;
                case "3":
                    CRD(AskString());
                    break;
                case "4":
                    Dir(AskString());
                    break;

                default:
                    Console.Clear();
                    break;
            }
        }
        static string AskString()
        {
            return Console.ReadLine();
        }
        static void Find()
        {
            Console.WriteLine("Dirctori que bols busacar");
            Console.WriteLine(Directory.Exists(AskString()));
            EndProgram();
        }
        static void Ensenyar(string path)
        {
            List<string> directoris = new List<string>(Directory.EnumerateDirectories(path));
            directoris.AddRange(Directory.EnumerateFiles(path));
            directoris.ForEach(direcoti => Console.WriteLine(direcoti));
            EndProgram();
        }
        static void CRD(string path)
        {
            bool flag;
            do
            {
                flag = false;
                switch (AskString())
                {
                    case "Delete":
                        Delete(path);
                        break;
                    case "Reneme":
                        Rename(path);
                        break;
                    case "Create":
                        Create(path);
                        break;
                    default:
                        flag = true;
                        break;
                }
            } while (flag);
            EndProgram();
        }
        static void Delete(string path)
        {
            if (Directory.Exists(path))
            {
                Directory.Delete(path);
                Console.WriteLine("El directori se a esborat corctament");
            }
        }
        static void Rename(string path)
        {
            string newPath = NewPath(path.Split(@"\"));
            if (Directory.Exists(path))
            {
                Console.WriteLine("Quin nom vols");
                Directory.Move(path, newPath + AskString());
            }
        }
        static string NewPath(string[] arrayPath)
        {
            string newPath = "";
            for (int i = 0; i < arrayPath.Length - 1; i++)
            {
                newPath += arrayPath[i] + @"\";
            }
            return newPath;
        }
        static void Create(string path)
        {
            string name = AskString();
            Directory.CreateDirectory(path + @"\" + name);
            Console.WriteLine($"The directory ha estat creat {Directory.GetCreationTime(path + @"\" + name)}.");
        }
        static void Dir(string path)
        {
            List<string> directoris = new List<string>();
            if (directoris.Count == 0)
            {
                directoris.AddRange(Directory.EnumerateDirectories(path));
                directoris.AddRange(Directory.EnumerateFiles(path));
            }
            foreach (string directorio in directoris)
            {
                if (Directory.Exists(directorio))
                {
                    Console.WriteLine($"<DIR> {directorio}, {Directory.GetLastAccessTime(directorio)}");
                    Dir(directorio);
                }
                if (File.Exists(directorio))
                {
                    Console.WriteLine($"<FILE> {directorio}, {File.GetLastAccessTime(directorio)}");
                }
            }
            
        }
        static void EndProgram()
        {
            Console.ReadLine();
            Console.Clear();
        }
    }
}

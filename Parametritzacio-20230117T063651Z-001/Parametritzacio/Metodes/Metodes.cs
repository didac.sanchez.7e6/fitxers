﻿using System;
/*
* AUTHOR: Didac Sanchez Cullell
* DATE: 2023/01/10
*/


namespace Metodes
{
    class Metodes
    {
        static void Main(string[] args)
        {
            var menu = new Metodes();
            menu.Menu();
        }
        public void Menu()
        {
            string opcio;
            do
            {
                Console.WriteLine("0.Exit");
                Console.WriteLine("1.RigtTriangleSize");
                Console.WriteLine("2.Lamp");
                Console.WriteLine("3.CampSiteOrganizer");
                Console.WriteLine("4.BasicRobot");
                Console.WriteLine("5.ThreeInArow");
                Console.WriteLine("6.SquashCounter");
                opcio = Console.ReadLine();
                MenuOpcions(opcio);
            } while (opcio != "0");
        }
        public void MenuOpcions(string opcio)
        {
            switch (opcio)
            {
                case "0":
                    break;
                case "1":
                    RigtTriangleSize();
                    break;
                case "2":
                    Lamp();
                    break;
                case "3":
                    CAMPSITEORGANIZER();
                    break;
                case "4":
                    BasicRobot();
                    break;
                case "5":
                    ThreeInArow();
                    break;
                case "6":
                    SquashCounter();
                    break;
                default:
                    Console.Clear();
                    break;
            }
        }
        public void RigtTriangleSize()
        {
            Console.Clear();
            Console.WriteLine("Quants triangles vols calcular");
            int numTriangles = (int)Math.Floor(AskNumero());
            double[] resultats = new double[numTriangles * 2];
            for (int i = 0; i < resultats.Length; i += 2)
            {
                Console.WriteLine("Quina es la base");
                double trianglrBase = AskNumero();
                Console.WriteLine("Quina es la altura");
                double trianglrAltura = AskNumero();
                resultats[i] = trianglrBase;
                resultats[i + 1] = trianglrAltura;
            }
            for (int i = 0; i < resultats.Length; i += 2)
            {
                Console.WriteLine($"Un triangle de {resultats[i]} x {resultats[i + 1]} té {resultats[i] * resultats[i + 1]} d'àrea i XX de perímetre.");
            }
            Console.ReadLine();
            Console.Clear();
        }
        public void Lamp()
        {
            Console.Clear();
            bool lamp = false;
            string acion;
            do
            {
                Console.WriteLine("Que vas a hacer");
                acion = AskString();
                if (acion == "TURN OFF")
                {
                    lamp = false;
                    Console.WriteLine(lamp);
                }
                if (acion == "TURN ON")
                {
                    lamp = true;
                    Console.WriteLine(lamp);
                }
                if (acion == "TOGGLE")
                {
                    lamp = !lamp;
                    Console.WriteLine(lamp);
                }
            } while (acion != "END");
            EndProgram();
        }
  
        public void CAMPSITEORGANIZER()
        {
            int personas = 0, parcelas = 0;
            string movimiento, lista = "";
            Console.Clear();
            do
            {
                Console.WriteLine("Que esta pasando");
                movimiento = AskString();
                string[] aciones = movimiento.Split(" ");
                if (aciones[0] == "ENTRA")
                {
                    personas += Convert.ToInt32(aciones[1]);
                    parcelas++;
                    Console.WriteLine($"parcel·les: {parcelas} \n persones: {personas}");
                    lista += aciones[1] + " " + aciones[2] + ",";
                }
                if (aciones[0] == "MARXA")
                {

                    string[] subLista = lista.Split(",");

                    int[] persones = new int[subLista.Length - 1];
                    string[] noms = new string[subLista.Length - 1];
                    for (int i = 0; i < subLista.Length - 1; i++)
                    {
                        string[] subSubLista = subLista[i].Split(" ");
                        for (int j = 0; j < subSubLista.Length; j++)
                        {
                            if (j == 0)
                            {
                                persones[i] = Convert.ToInt32(subSubLista[j]);
                            }
                            if (j == 1)
                            {
                                noms[i] = subSubLista[j];
                            }
                        }
                    }
                    for (int i = 0; i < noms.Length; i++)
                    {
                        if (noms[i] == aciones[1])
                        {
                            personas -= persones[i];
                            persones[i] = 0;
                            noms[i] = "";
                            parcelas--;
                            break;
                        }
                        if (i == noms.Length - 1)
                        {
                            Console.WriteLine("No hi ha presones mab aquet nom");
                        }
                    }
                    Console.WriteLine($"parcel·les: {parcelas} \n persones: {personas}");
                    lista = "";
                    for (int i = 0; i < noms.Length; i++)
                    {
                        if (persones[i] != 0)
                        {
                            lista += persones[i] + " " + noms[i] + ",";
                        }
                    }

                }
            } while (movimiento != "END");
            EndProgram();
        }
        public void BasicRobot()
        {
            Console.Clear();
            string acion;
            double velociada = 1, posicionHorizontal = 0, posicionVertical = 0;
            do
            {
                acion = AskString();
                if (acion == "POSICIO") Console.WriteLine($"La posició del robot és({posicionHorizontal}, {posicionVertical})");
                if (acion == "DALT") posicionVertical += velociada;
                if (acion == "BAIX") posicionVertical -= velociada;
                if (acion == "DRETA") posicionHorizontal += velociada;
                if (acion == "ESQUERRA") posicionHorizontal -= velociada;
                if (acion == "ACELERAR" && velociada < 10) velociada += 0.5;
                if (acion == "DISMINUIR" && velociada > 0) velociada -= 0.5;
                if (acion == "VELOCITAT") Console.WriteLine($"La velocitat del robot és {velociada}");

            } while (acion != "END");
            EndProgram();
        }
        public void ThreeInArow()
        {
            char[,] tablero = { { '.', '.', '.' }, { '.', '.', '.' }, { '.', '.', '.' } };
            bool flag = true;
            int turnos = 0;
            do
            {
                Console.Clear();
                ImprimirMatriz(tablero);
                string posicion = AskString();
                string[] cordenada = posicion.Split();
                if (tablero[Convert.ToInt32(cordenada[0]), Convert.ToInt32(cordenada[1])] != '.')
                {
                    Console.WriteLine("La posisio ja esta agafada");
                    Console.ReadLine();
                }
                if (turnos % 2 == 0 && tablero[Convert.ToInt32(cordenada[0]), Convert.ToInt32(cordenada[1])] == '.')
                {
                    tablero[Convert.ToInt32(cordenada[0]), Convert.ToInt32(cordenada[1])] = 'X';
                    turnos++;
                }
                if (turnos % 2 != 0 && tablero[Convert.ToInt32(cordenada[0]), Convert.ToInt32(cordenada[1])] == '.')
                {
                    tablero[Convert.ToInt32(cordenada[0]), Convert.ToInt32(cordenada[1])] = 'O';
                    turnos++;
                }
                for (int i = 0; i < tablero.GetLength(0); i++)
                {
                    if (tablero[i, 0] == tablero[i, 1] && tablero[i, 1] == tablero[i, 2] && tablero[i, 2] != '.' && tablero[i, 1] != '.' && tablero[i, 0] != '.')
                    {
                        flag = false;
                        Console.WriteLine($"Ha guanyat las {tablero[i, 0]}");
                        break;
                    }
                    if (tablero[0, i] == tablero[1, i] && tablero[1, i] == tablero[2, i] && tablero[2, i] != '.' && tablero[1, i] != '.' && tablero[0, i] != '.')
                    {
                        flag = false;
                        Console.WriteLine($"Ha guanyat las {tablero[0, i]}");
                        break;
                    }
                }
                if (tablero[0, 0] == tablero[1, 1] && tablero[1, 1] == tablero[2, 2] && tablero[2, 2] != '.' && tablero[1, 1] != '.' && tablero[0, 0] != '.')
                {
                    flag = false;
                    Console.WriteLine($"Ha guanyat las {tablero[1, 1]}");
                    break;
                }
                if (tablero[2, 0] == tablero[1, 1] && tablero[1, 1] == tablero[0, 2] && tablero[0, 2] != '.' && tablero[1, 1] != '.' && tablero[2, 0] != '.')
                {
                    flag = false;
                    Console.WriteLine($"Ha guanyat las {tablero[1, 1]}");
                    break;
                }
                if (turnos == 9)
                {
                    Console.WriteLine("Enpat");
                    break;
                }
            } while (flag);
            EndProgram();
        }
        public void SquashCounter()
        {
            Console.Clear();
            Console.WriteLine("Quans sets se han fet");
            int sets = Convert.ToInt32(AskNumero()), guanyatA = 0, guanyatB = 0;
            char treu = 'C';
            for (int i = 0; i < sets; i++)
            {
                int puntsA = 0, puntsB = 0;
                if (guanyatA == 3 || guanyatB == 3) break;
                string punsDelSet = AskString();
                foreach (char punt in punsDelSet)
                {
                    if (punt  == 'A')
                    {
                        puntsA++;
                    }
                    if (punt == 'B')
                    {
                        puntsB++;
                    }
                    if(puntsB>=9 && puntsB - puntsA >= 2)
                    {
                        if (treu == 'B')
                        {
                            guanyatB++;
                        }
                        treu = 'B';
                        break;
                    }
                    if (puntsA >= 9 && puntsA - puntsB >= 2)
                    {
                        if (treu == 'A')
                        {
                            guanyatA++;
                        }
                        treu = 'A';
                        break;
                    }
                    if (punt == 'F') break;
                }
                Console.Write($"{puntsA} - {puntsB}\t");
                if (guanyatA > 0 || guanyatB > 0) Console.Write($"{guanyatA} - {guanyatB}");
                Console.WriteLine("");
            }
            EndProgram();
        }
        double AskNumero()
        {
            double numero = Convert.ToDouble(Console.ReadLine());
            return numero;
        }
        string AskString()
        {
            string frase = Console.ReadLine();
            frase = frase.ToUpper();
            return frase;
        }
        public void EndProgram()
        {
            Console.ReadLine();
            Console.Clear();
        }
        public void ImprimirMatriz(char[,] matriz)
        {
            for (int i = 0; i < matriz.GetLength(0); i++)
            {
                for (int j = 0; j < matriz.GetLength(1); j++)
                {
                    Console.Write($"{matriz[i, j]} \t");
                    if (j == matriz.GetLength(1) - 1) Console.WriteLine("");
                }
            }
        }
    }
}
